require_relative '../app/person'
require 'test/unit'


class PersonTest < Test::Unit::TestCase

  $john = Person.new(last_name: "Ferguson", first_name: "John", gender: "Male",
                     birth_date: "05/30/1983", favorite_color: "Blue")
  $matz = Person.new(last_name: "Matsumoto", first_name: "Yukihiro",
                     gender: "Male", birth_date: "04/14/1965",
                     favorite_color: "Ruby")
  $ada = Person.new(last_name: "Lovelace", first_name: "Ada", gender: "Female",
                    birth_date: "12/10/1815", favorite_color: "Turquoise")
  $carly = Person.new(last_name: "Jepsen", first_name: "Carly Rae",
                      gender: "Female", birth_date: "11/21/1985",
                      favorite_color: "Orange")

  def test_last_name
    assert_equal "Matsumoto", $matz.last_name
  end

  def test_first_name
    assert_equal "Yukihiro", $matz.first_name
  end

  def test_gender
    assert_equal "Male", $matz.gender
  end

  def test_birth_date
    assert_equal "04/14/1965", $matz.birth_date.strftime('%m/%d/%Y')
  end

  def test_favorite_color
    assert_equal "Ruby", $matz.favorite_color
  end

  def test_all
    assert_equal [$john, $matz, $ada, $carly], Person.all
  end

  def test_ordered_attributes
    assert_equal "Ferguson John Male 05/30/1983 Blue", $john.ordered_attributes
  end

  def test_sort_all_by_gender_then_last_name
    assert_equal [$carly, $ada, $john, $matz], Person.sort_all_by_gender_then_last_name
  end

  def test_sort_all_by_birth_date
    assert_equal [$ada, $matz, $john, $carly], Person.sort_all_by_birth_date
  end

  def test_method_missing_not_starting_with_sort_all_by_raises_no_method_error
    assert_raise NoMethodError do
     Person.does_not_start_with_sort_all_by
   end
  end

  def test_sort_all_by_non_existing_attribute_raises_no_method_error
    assert_raise NoMethodError do
      Person.sort_all_by_non_existing_attribute
    end
  end

  def test_sort_all_by_existing_attribute_ascending
    assert_equal [$john, $carly, $ada, $matz], Person.sort_all_by_last_name
  end

  def test_sort_all_by_existing_attribute_descending
    assert_equal [$matz, $ada, $carly, $john], Person.sort_all_by_last_name("desc")
  end

end