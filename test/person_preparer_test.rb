require_relative '../app/person_preparer.rb'
require 'test/unit'

class PersonPreparerTest < Test::Unit::TestCase

  def test_collect_lines_from_files
    assert_equal ["Abercrombie, Neil, Male, Tan, 2/13/1943\n",
                  "Bishop, Timothy, Male, Yellow, 4/23/1967\n",
                  "Kelly, Sue, Female, Pink, 7/12/1959"],
                  PersonPreparer.collect_lines_from_files("../data/comma.txt")
  end

  def test_batch_initialize_from_files
    assert_equal ([{:birth_date=>"2/13/1943", :favorite_color=>"Tan",
                  :first_name=>"Neil", :gender=>"Male", :last_name=>"Abercrombie"},
                  {:birth_date=>"3/3/1985", :favorite_color=>"Red",
                  :first_name=>"Steve", :gender=>"M", :last_name=>"Smith"},
                  {:birth_date=>"6/3/1975", :favorite_color=>"Red",
                  :first_name=>"Anna", :gender=>"F", :last_name=>"Kournikova"}]),
                  PersonPreparer.batch_initialize_from_files(Person, "fixtures/batch_initialize_data.txt")
  end

  def test_prepare_all
    assert_equal ([{:birth_date=>"2/13/1943", :favorite_color=>"Tan",
                  :first_name=>"Neil", :gender=>"Male", :last_name=>"Abercrombie"},
                  {:birth_date=>"3-3-1985", :favorite_color=>"Red",
                  :first_name=>"Steve", :gender=>"M", :last_name=>"Smith"},
                  {:birth_date=>"6-3-1975", :favorite_color=>"Red",
                  :first_name=>"Anna", :gender=>"F", :last_name=>"Kournikova"}]),
                  PersonPreparer.prepare_all(["Abercrombie, Neil, Male, Tan, 2/13/1943\n",
                                            "Smith | Steve | D | M | Red | 3-3-1985\n",
                                            "Kournikova Anna F F 6-3-1975 Red"])
  end

  def test_prepare
    assert_equal ({last_name: "Abercrombie", first_name: "Neil", gender: "Male",
                  birth_date: "2/13/1943", favorite_color: "Tan"}),
                  PersonPreparer.prepare("Abercrombie, Neil, Male, Tan, 2/13/1943\n")
    assert_equal ({last_name: "Smith", first_name: "Steve", gender: "M",
                  birth_date: "3-3-1985", favorite_color: "Red"}),
                  PersonPreparer.prepare("Smith | Steve | D | M | Red | 3-3-1985\n")
    assert_equal ({last_name: "Kournikova", first_name: "Anna", gender: "F",
                  birth_date: "6-3-1975", favorite_color: "Red"}),
                  PersonPreparer.prepare("Kournikova Anna F F 6-3-1975 Red\n")
  end

  def test_correct_spacing
    assert_equal ("Abercrombie Neil Male Tan 2/13/1943"),
                  PersonPreparer.correct_spacing("Abercrombie, Neil, Male, Tan, 2/13/1943")
    assert_equal ("Smith Steve D M Red 3-3-1985"),
                  PersonPreparer.correct_spacing("Smith | Steve | D | M | Red | 3-3-1985")
    assert_equal ("Kournikova Anna F F 6-3-1975 Red"),
                  PersonPreparer.correct_spacing("Kournikova Anna F F 6-3-1975 Red")
  end

  def test_remove_middle_initial
    assert_equal (["Abercrombie", "Neil", "Male", "Tan", "2/13/1943"]),
                  PersonPreparer.remove_middle_initial("Abercrombie Neil Male Tan 2/13/1943")
    assert_equal (["Smith", "Steve", "M", "Red", "3-3-1985"]),
                  PersonPreparer.remove_middle_initial("Smith Steve D M Red 3-3-1985")
    assert_equal (["Kournikova", "Anna", "F", "6-3-1975", "Red"]),
                  PersonPreparer.remove_middle_initial("Kournikova Anna F F 6-3-1975 Red")
  end

  def test_correct_birthdate_position
    assert_equal (["Abercrombie", "Neil", "Male", "2/13/1943", "Tan"]),
                  PersonPreparer.correct_birthdate_position(["Abercrombie", "Neil", "Male", "Tan", "2/13/1943"])
    assert_equal (["Smith", "Steve", "M", "3-3-1985", "Red"]),
                  PersonPreparer.correct_birthdate_position(["Smith", "Steve", "M", "Red", "3-3-1985"])
    assert_equal (["Kournikova", "Anna", "F", "6-3-1975", "Red"]),
                  PersonPreparer.correct_birthdate_position(["Kournikova", "Anna", "F", "6-3-1975", "Red"])
  end

  def test_map_to_hash
    assert_equal ({last_name: "Abercrombie", first_name: "Neil", gender: "Male",
                  birth_date: "2/13/1943", favorite_color: "Tan"}),
                  PersonPreparer.map_to_hash(["Abercrombie", "Neil", "Male", "2/13/1943", "Tan"])
    assert_equal ({last_name: "Smith", first_name: "Steve", gender: "M",
                  birth_date: "3-3-1985", favorite_color: "Red"}),
                  PersonPreparer.map_to_hash(["Smith", "Steve", "M", "3-3-1985", "Red"])
    assert_equal ({last_name: "Kournikova", first_name: "Anna", gender: "F",
                  birth_date: "6-3-1975", favorite_color: "Red"}),
                  PersonPreparer.map_to_hash(["Kournikova", "Anna", "F", "6-3-1975", "Red"])
  end

end
