require_relative '../app/normalizable'
require 'test/unit'

class NormalizableTest < Test::Unit::TestCase

  def setup
    @test_obj = Object.new
    @test_obj.extend(Normalizable)
  end

  def test_normalize_gender
    assert_equal "Male", @test_obj.normalize_gender("M")
    assert_equal "Male", @test_obj.normalize_gender("Male")
    assert_equal "Female", @test_obj.normalize_gender("F")
    assert_equal "Female", @test_obj.normalize_gender("Female")
  end

  def test_normalize_date
    assert_equal Date.strptime("05/05/2013", "%m/%d/%Y"),
                 @test_obj.normalize_date("05-05-2013")
    assert_equal Date.strptime("05/05/2013", "%m/%d/%Y"),
                 @test_obj.normalize_date("5-5-2013")
    assert_equal Date.strptime("05/05/2013", "%m/%d/%Y"),
                 @test_obj.normalize_date("5/5/2013")
    assert_equal Date.strptime("05/05/2013", "%m/%d/%Y"),
                 @test_obj.normalize_date("05/05/2013")
  end

end