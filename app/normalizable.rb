require 'date'

module Normalizable

  def normalize_gender(gender)
    case gender
    when "M" then gender = "Male"
    when "F" then gender = "Female"
    end
    gender
  end

  def normalize_date(date)
    date.gsub!("-", "/")
    Date.strptime(date, "%m/%d/%Y")
  end

end