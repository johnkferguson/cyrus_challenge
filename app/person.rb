require_relative 'normalizable'

class Person
  include Normalizable

  @attributes = [ :last_name, :first_name, :gender, :birth_date, :favorite_color ]
  attr_reader *@attributes

  PEOPLE = []

  def initialize(last_name: nil, first_name: nil, gender: nil,
                 birth_date: nil, favorite_color: nil)
    @last_name = last_name if last_name
    @first_name = first_name if first_name
    @gender = normalize_gender(gender) if gender
    @birth_date = normalize_date(birth_date) if birth_date
    @favorite_color = favorite_color if favorite_color
    PEOPLE << self
  end

  def self.all
    PEOPLE
  end

  def ordered_attributes
    "#{last_name} #{first_name} #{gender} #{birth_date.strftime('%m/%d/%Y')} #{favorite_color}"
  end

  # Defines class methods to sort all instances of a class by any attribute. By passing in "desc" as the
  # optional argument, the output can be sorted in descending order.
  @attributes.each do |attr|
    self.define_singleton_method("sort_all_by_#{attr}") do |arg=nil|
      sorted = all.sort_by { |p| p.send(attr) }
      arg == "desc" ? sorted.reverse : sorted
    end
  end

  def self.sort_all_by_gender_then_last_name
    self.all.sort_by { |p| [p.gender, p.last_name] }
  end

end