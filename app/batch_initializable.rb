module BatchInitializable

  def batch_initialize_from_files(klass, *files)
    lines_array = collect_lines_from_files(*files)
    prepared_lines_array = prepare_all(lines_array)
    prepared_lines_array.each { |p| klass.new (p) }
  end

  def collect_lines_from_files(*relative_file_paths)
    lines_array = []
    relative_file_paths.each do |file_path|
      file = File.new("#{file_path}")
      file.each { |line| lines_array << line }
    end
    lines_array
  end

  def prepare_all(array)
    array.collect { |string| prepare(string) }
  end

end