require_relative 'app/person'
require_relative 'app/person_preparer'

task :default => [:test]

task :test do
  cd "test/"
  ruby "person_preparer_test.rb --verbose"
  ruby "person_test.rb --verbose"
  ruby "normalizable_test.rb --verbose"
end

namespace :print do
  PersonPreparer.batch_initialize_from_files(Person, "data/comma.txt", "data/pipe.txt", "data/space.txt")

  desc "Print records sorted by gender (female first), then by last name ascending."
  task :output_1 do
    puts "Output 1: records sorted by gender (female first), then by last name ascending."
    Person.sort_all_by_gender_then_last_name.each do |p|
      puts p.ordered_attributes
    end
  end

  desc "Print records sorted by birth date, ascending."
  task :output_2 do
    puts "Output 2: records sorted by birth date, ascending."
    Person.sort_all_by_birth_date.each do |p|
      puts p.ordered_attributes
    end
  end

  desc "Print records sorted by last name, descending."
  task :output_3 do
    puts "Output 3: records sorted by last name, descending."
    Person.sort_all_by_last_name("desc").each do |p|
      puts p.ordered_attributes
    end
  end

  desc "Print all three outputs"
  task :output_all do
    Rake::Task['print:output_1'].invoke
    puts ""
    Rake::Task['print:output_2'].invoke
    puts ""
    Rake::Task['print:output_3'].invoke
  end

end